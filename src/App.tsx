import React from 'react';
import logo from './logo.svg';
import './App.css';
import { useDispatch, useSelector } from 'react-redux';
import { programs, fetchCatalogue, isLoading } from './features/catalogue/reducers';

function App() {
  const dispatch = useDispatch();
  const fullCatalogue = useSelector(programs);
  const isLoadingCatalogue = useSelector(isLoading);

  return (
    <div className="App">
      <button onClick={() => { dispatch(fetchCatalogue()) }}>Fetch catalogue!</button>
      { isLoadingCatalogue && <h1>Loading...</h1> }
      { fullCatalogue && fullCatalogue.length > 0 &&
        <div className="div__catalogue">
          { fullCatalogue.map(program => <div key={program.id}><h1>{program.name}</h1> </div>) }
        </div>
      }
    </div>
  );
}

export default App;
