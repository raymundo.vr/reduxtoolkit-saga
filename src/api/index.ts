import { Program } from "../app/types";

export const fetchCatalogueAPI = (): Promise<Program[]> => new Promise((resolve, _) => {
    setTimeout(() => { resolve([
        { id: 'parent', name: 'Main Program', restricted: ['user-a', 'user-b'], subPrograms: ['child-a', 'child-b'] },
        { id: 'child-a', name: 'Child A Program' },
        { id: 'child-b', name: 'Child B Program' },
        { id: 'free', name: 'Free Program' },
    ]) }, 500);
});
