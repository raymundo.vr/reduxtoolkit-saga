import { combineReducers, configureStore, getDefaultMiddleware } from '@reduxjs/toolkit';
import createSagaMiddleware from '@redux-saga/core';
import catalogueReducer from '../features/catalogue/reducers';
import watcherSaga from '../features/catalogue/sagas';

const sagaMiddleware = createSagaMiddleware();

const store = configureStore({
    reducer: combineReducers({ catalogue: catalogueReducer }),
    middleware: [sagaMiddleware]
});

sagaMiddleware.run(watcherSaga);

export type RootState = ReturnType<typeof store.getState>;

export default store;
