export interface Program {
    id: string;
    name: string;
    subPrograms?: string[];
    restricted?: string[];
}