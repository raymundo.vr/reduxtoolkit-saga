import { createSlice } from "@reduxjs/toolkit";
import { RootState } from "../../app/store";
import { Program } from "../../app/types";

interface OwnState {
    isLoading: boolean;
    programs: Program[];
}

const initialState: OwnState = {
    isLoading: false,
    programs: [],
}

export const catalogueSlice = createSlice({
    name: 'catalogue',
    initialState,
    reducers: {
        fetchCatalogue: (state) => { return { ...state, isLoading: true, } },
        catalogueFetched: (state, action) => {
            return { ...state, programs: action.payload.catalogue, isLoading: false };
        }
    },
});

export const { catalogueFetched, fetchCatalogue } = catalogueSlice.actions;

export const programs = (state: RootState) => state.catalogue.programs;
export const isLoading = (state: RootState) => state.catalogue.isLoading;

export default catalogueSlice.reducer;

