import { call, put, takeLatest } from 'redux-saga/effects';
import { fetchCatalogue, catalogueFetched } from './reducers';
import { fetchCatalogueAPI } from '../../api/index';
import { Program } from '../../app/types';

export function* getCatalogue() {
    const response: Program[] = yield call(fetchCatalogueAPI);
    yield put(catalogueFetched({ catalogue: response }));
}

export default function* watcherSaga() {
    yield takeLatest(fetchCatalogue.type, getCatalogue);
}